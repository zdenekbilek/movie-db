import { NgModule } from '@angular/core';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [MatGridListModule, MatChipsModule, MatInputModule, MatIconModule, MatButtonModule ],
  exports: [MatGridListModule, MatChipsModule, MatInputModule, MatIconModule, MatButtonModule ],
})
export class MaterialModule { }
