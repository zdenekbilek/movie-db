import { AfterContentInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatGridList } from '@angular/material/grid-list';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import get from 'lodash.get';
import { Query } from 'sift';

import { Movie } from '../shared/movie.model';
import * as fromMovie from '../shared/movie.reducer';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.sass']
})
export class MovieListComponent implements AfterContentInit, OnInit {
  @ViewChild('grid') grid: MatGridList;

  gridByBreakpoint = {
    xl: 8,
    lg: 6,
    md: 5,
    sm: 3,
    xs: 2
  };

  filter$: Observable<Query<Movie>>;
  movies$: Observable<Movie[]>;

  constructor(
    private mediaObserver: MediaObserver,
    private store: Store<fromMovie.State>,
    private route: ActivatedRoute
  ) {}

  /**
   * Subscribe to filter in params and update a movie list whenever the filter changes
   */
  ngOnInit(): void {
    this.filter$ = this.route.queryParams.pipe(map(params => {
      try {
        return JSON.parse(get(params, 'filter', '{}'));
      } catch {
        return {};
      }
    }));
    this.filter$.subscribe((filterQuery) => {
      this.movies$ = this.store.pipe(select(fromMovie.getFilteredMovies, filterQuery));
    });
  }

  /**
   * Subscribe to MediaObserver, so columns might be adjusted as different media queries are activated
   */
  ngAfterContentInit(): void {
    this.mediaObserver.asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      ).subscribe((change: MediaChange) => {
        if (this.grid) {
          this.grid.cols = this.gridByBreakpoint[change.mqAlias];
        }
      });
  }
}
