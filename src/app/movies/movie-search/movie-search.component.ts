import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import get from 'lodash.get';
import debounce from 'lodash.debounce';
import { Query } from 'sift';

import { Genres, GenreType, Movie } from '../shared/movie.model';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class MovieSearchComponent implements OnInit {
  genreOptions = Genres;
  filter: Query<Movie>;
  term: string;
  genreSelected: Set<GenreType>;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.search = debounce(this.search, 300);
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(({ filter = '{}' }) => {
      this.filter = this.parseJsonFilter(filter);
    });
    const filterQuery = this.parseJsonFilter(get(this.route.snapshot.queryParams, 'filter', '{}'));
    this.term = get(filterQuery, 'name.$regex', '');
    this.genreSelected = new Set(get(filterQuery, 'genres.$in', []));
  }

  search(term: string): void {
    this.updateFilter({
      name: {
        $regex: term,
        $options: 'i'
      }
    });
  }

  toggleGenre(genre: GenreType): void {
    if (this.genreSelected.has(genre)) {
      this.genreSelected.delete(genre);
    } else {
      this.genreSelected.add(genre);
    }
    this.updateFilter({
      genres: {
        $in: this.genreSelected.size ? [...this.genreSelected] : Object.values(this.genreOptions),
      }
    });
  }

  updateFilter(query: Query<Movie>): void {
    this.router.navigate([], {
      queryParams: {
        filter: JSON.stringify({
          ...this.filter,
          ...query,
        }),
      }
    });
  }

  parseJsonFilter(json: string): Query<Movie> {
    try {
      const filter = JSON.parse(json);
      return filter;
    } catch {
      return {
        genres: {
          $in: Object.values(this.genreOptions),
        }
      };
    }
  }
}
