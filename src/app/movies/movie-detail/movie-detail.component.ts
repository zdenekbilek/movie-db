import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Movie } from '../shared/movie.model';
import { RouterExtService } from '../shared/router-ext.service';
import * as fromMovie from '../shared/movie.reducer';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.sass']
})
export class MovieDetailComponent implements OnInit {
  movie$: Observable<Movie>;

  constructor(
    private routerExtService: RouterExtService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<fromMovie.State>,
  ) { }

  ngOnInit(): void {
    this.getMovie();
  }

  getMovie(): void {
    const id: string = this.activatedRoute.snapshot.paramMap.get('id');
    this.movie$ = this.store.select(fromMovie.getMovieById, { id });
  }

  goBack(): void {
    const previous = this.routerExtService.getPreviousUrl();
    if (previous === this.routerExtService.getCurrentUrl()) {
      this.router.navigate(['/movies']);
    } else {
      this.location.back();
    }
  }
}
