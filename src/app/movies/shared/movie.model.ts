export enum Genres {
  Action = 'action',
  Adventure = 'adventure',
  Biography = 'biography',
  Comedy = 'comedy',
  Crime = 'crime',
  Drama = 'drama',
  History = 'history',
  Mystery = 'mystery',
  Scifi = 'scifi',
  Sport = 'sport',
  Thriller = 'thriller'
}

export type GenreType = 'action' | 'adventure' | 'biography' | 'comedy' | 'crime' | 'drama' | 'history' | 'mystery' | 'scifi' | 'sport' | 'thriller';

export interface Movie {
  id: number;
  key: string;
  name: string;
  description: string;
  genres: Array<GenreType>;
  rate: string;
  length: string;
  img: string;
}

export interface MoviesMap {
  [key: string]: Movie;
}
