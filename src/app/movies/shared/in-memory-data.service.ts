import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { MOVIES } from './mock-movies';
import { Movie } from './movie.model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb(): { movies: Movie[] } {
    const movies = MOVIES;
    return { movies };
  }
}
