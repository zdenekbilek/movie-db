import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import sift, { Query } from 'sift';

import { getMoviesSuccess, MovieActionTypes } from './movie.actions';
import { Movie, MoviesMap } from './movie.model';

export const moviesFeatureKey = 'movies';

export interface State extends EntityState<Movie> {}

export const adapter: EntityAdapter<Movie> = createEntityAdapter<Movie>();

export const initialState: State = adapter.getInitialState();

const moviesReducer = createReducer(
  initialState,
  on(getMoviesSuccess, (state, { movies }) => {
    return adapter.setAll(movies, state);
  })
);

export function reducer(state: State | undefined, action: Action): State {
  return moviesReducer(state, action);
}

// Selectors
export const getMoviesState = createFeatureSelector<State>('movies');

export const {
  selectAll: getAllMovies,
  selectEntities: getAllMovieEntities
} = adapter.getSelectors(getMoviesState);

export const getFilteredMovies = createSelector(
  getAllMovies,
  (movies: Movie[], filter: Query<Movie>): Movie[] => movies.filter(sift(filter)),
);

export const getMovieById = createSelector(
  getAllMovieEntities,
  (movies: MoviesMap, props: { id: string }): Movie => movies[props.id]
);
