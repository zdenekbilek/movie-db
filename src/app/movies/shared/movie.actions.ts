import { createAction, props } from '@ngrx/store';
import { Movie } from './movie.model';

export enum MovieActionTypes {
  GET_MOVIES = 'GET_MOVIES',
  GET_MOVIES_SUCCESS = 'GET_MOVIES_SUCCESS',
}

export const getMovies = createAction(MovieActionTypes.GET_MOVIES);
export const getMoviesSuccess = createAction(MovieActionTypes.GET_MOVIES_SUCCESS, props<{movies: Movie[]}>());
