import { Injectable } from '@angular/core';
import { Effect, Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { getMoviesSuccess, MovieActionTypes } from './movie.actions';
import { MovieService } from './movie.service';

@Injectable()
export class MovieEffects {
  getMovies$ = createEffect(() => this.actions$.pipe(
    ofType(MovieActionTypes.GET_MOVIES),
    mergeMap(() => this.movieService.getAll()
      .pipe(
        map(movies => getMoviesSuccess({ movies })),
        catchError(() => EMPTY)
      ))
    )
  );

  constructor(
    private movieService: MovieService,
    private actions$: Actions
  ) {}
}
