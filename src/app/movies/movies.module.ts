import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MoviesRoutingModule } from './movies-routing.module';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import * as fromMovie from './shared/movie.reducer';
import { MaterialModule } from '../material.module';
import { MovieSearchComponent } from './movie-search/movie-search.component';
import { MoviesComponent } from './movies.component';
import { MovieService } from './shared/movie.service';
import { MovieEffects } from './shared/movie.effect';
import { InMemoryDataService } from './shared/in-memory-data.service';


@NgModule({
  declarations: [MoviesComponent, MovieListComponent, MovieDetailComponent, MovieSearchComponent],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromMovie.moviesFeatureKey, fromMovie.reducer),
    EffectsModule.forFeature([MovieEffects]),
    // Mock API
    HttpClientInMemoryWebApiModule.forFeature(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    FlexLayoutModule
  ],
  providers: [MovieService]
})
export class MoviesModule { }
