import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { getMovies } from './shared/movie.actions';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html'
})
export class MoviesComponent implements OnInit {
  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(getMovies());
  }
}
